# scope-pi

> A case for a rpi that attaches to a telescope

View on [openjscad.org](http://openjscad.org/#https://gitlab.com/johnwebbcole/scope-pi/raw/master/dist/scope-pi.jscad)

## Development

The jscad project `scope-pi` uses gulp to create a `dist/scope-pi.jscad` file and watches your source for changes. You can drag the `dist/scope-pi.jscad` directory into the drop area on [openjscad.org](http://openjscad.org). Make sure you check `Auto Reload` and any time you save, gulp will recreate the `dist/scope-pi.jscad` file and your model should refresh.

## License

ISC © [John Cole](http://github.com/)
